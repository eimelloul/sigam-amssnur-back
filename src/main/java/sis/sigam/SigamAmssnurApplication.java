package sis.sigam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SigamAmssnurApplication {

	public static void main(String[] args) {
		SpringApplication.run(SigamAmssnurApplication.class, args);
	}

}
